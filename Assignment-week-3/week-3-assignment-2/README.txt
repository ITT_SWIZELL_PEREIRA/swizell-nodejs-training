The objective of this assignment is to create a server to process POST request to save the data that a user enters in an array and also create folders and file 
and save data in that file based on user input. (without Express)
for eg: if user enters country/state/name as usa/boston/swizell then a file about.txt in folder usa/boston/swizell will be created.

The server was created using createServer method in http module. The user has to enter the data through UI. The request sent is POST. I used fetch to send the request
to the server. I made a separate folder UserData to store the about files of users. 

In this assignment i have created the files in typescript and used gulp task handler to compile the ts files.
I used webpack to bundle my client code.

Instructions:
1)In command line pass gulp to compile the ts files. 
2)Then pass webpack for bundling.
3)To observe live changes in the file run  npm run start

 