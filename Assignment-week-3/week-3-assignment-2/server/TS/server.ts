import { createServer, IncomingMessage, ServerResponse } from 'http';
import { parse } from 'url';
import { Route } from './route'


export class Server {
    constructor(public port: number, public host: string) {
    }

    createServer() {
        const server: any = createServer(function (req: IncomingMessage, res: ServerResponse) {
            let parsedURL: any = parse(req.url, true);
            let path: any = parsedURL.pathname;
            path = path.replace(/^\/+|\/+$/g, "");
            //console.log(path);
            //let headers: any = req.headers;
            let method: any = req.method.toLowerCase();
            let reqBody: string = '';
            req.on('data', (chunk: string) => {
                reqBody += chunk;
            })
            req.on("end", function () {
                route(reqBody, path, res)
            });
        });
        server.listen(this.port, this.host, () => {
            console.log("Listening on port 1234");
        });
    }
}

let server = new Server(1234, 'localhost');
server.createServer();
function route(reqBody: any, path: string, res:ServerResponse) {
    switch (path) {
        case 'api/createUser': {
            let route = new Route(path, reqBody);
            route.createDirectory();
            route.createFile();
            let userDataArray = route.updateArray();
            let responseHeaderDetails = {
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*"
            }
            res.writeHead(200, responseHeaderDetails);
            res.write(userDataArray);
            res.end("\n");
        }
    }

}

