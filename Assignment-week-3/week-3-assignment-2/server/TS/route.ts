import * as fs from 'fs';

export class Route{
    routePath:string;
    reqBody:any;

    constructor(path:string, reqBody:object){
        this.routePath=path;
        this.reqBody=reqBody;
        this.reqBody = JSON.parse(this.reqBody);
        }

    createDirectory() {
       if(fs.existsSync(`./userData/${this.reqBody.country}/${this.reqBody.state}/${this.reqBody.name}`)){
            console.log('path exists');
        }
        else{
            fs.mkdirSync(`./userData/${this.reqBody.country}/${this.reqBody.state}/${this.reqBody.name}`, {recursive:true});
        }
    }

    createFile() {
        fs.writeFile(`./userData/${this.reqBody.country}/${this.reqBody.state}/${this.reqBody.name}/about.json`, `${this.reqBody.about}`, (err) => {
            if (err) throw err;
        })
    }

    updateArray() {
        var users = require('./user');
        users.push(this.reqBody);
        fs.writeFile('./server/TS/user.ts', `module.exports = ${JSON.stringify(users)}`, (err) => {
            if (err) throw err;
        })
        return JSON.stringify(users);
    }

}