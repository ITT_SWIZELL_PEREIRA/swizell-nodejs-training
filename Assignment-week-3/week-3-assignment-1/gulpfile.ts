var gulp = require("gulp");
var ts = require("gulp-typescript");
var tsProject = ts.createProject("tsconfig.json");

gulp.task("compile", function(){
    return gulp.src('TS/generator.ts').pipe(tsProject()).pipe(gulp.dest("JS"));
});