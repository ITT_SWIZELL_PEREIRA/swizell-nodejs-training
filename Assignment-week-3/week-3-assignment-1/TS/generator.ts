
import $ from "jquery"
$(document).ready(function(){
    let radioVarGendre:string='female';                                    //variable for storing radio value (gender)
    let selectVarNationality:string= 'FR' ;                                //for storing nationality
    let p:string= "";                                                     //to insert element h0 with result
    let generateAnotherName:string;                                        //to create a button

   /* This will check the radio that user clicks and save data*/ 
$("input[type='radio']").click(function(){
   // console.log($("input[name='gendre']:checked").val())
    radioVarGendre = `${$("input[name='gendre']:checked").val()}`;
});

/*This will check the change of the country and will update only on change*/
$('#nationality').on('change', function() {
    selectVarNationality = $('#nationality :selected').text();
});

fetchUserInfo(selectVarNationality, radioVarGendre);                  

/*using the async/wait mtd to handle async function */
async function fetchUserInfo(nationality:string, gender:string){ 
            const url:string ="https://randomuser.me/api/?gender="+gender+"&nat=" + nationality;

    try                                                                  
       { 
            const response:Response= await fetch(url);
            const userDataNameNation:any = await response.json();           //to get the json body from response
            $("h0").html("");                                             
            p = `<div>
            <h0 >${userDataNameNation.results[0].name.title}
            ${userDataNameNation.results[0].name.first}</h0>
            </div>`;
            $("#results").append(p);
            
        }catch(err){
           alert(err);
       }
    }

/*if you want to retain the same value for male and female then this is a button which by clicking you will just get new names  */
generateAnotherName = `<button id="loadmore" class = "btn btn-primary"> Generate Name </button>`;
$("#results").append(generateAnotherName);
$('#loadmore').on("click", ()=>{
fetchUserInfo(selectVarNationality, radioVarGendre);
});
})




