$(document).ready(function(){
    let radioVarGendre='female';                                    //variable for storing radio value (gender)
    let selectVarNationality= 'FR' ;                                //for storing nationality
    let Url = "";                                                   //setting default link that will show all females from france
    let p = "";                                                     //to insert element h0 with result
    let generateAnotherName;                                        //to create a button

  
/* This will check the radio that user clicks and save data*/ 
$("input[type='radio']").click(function(){
    radioVarGendre = $("input[name='gendre']:checked").val();
  //  console.log(radiovargendre);
});


/*This will check the change of the country and will update only on change*/
$('#nationality').on('change', function() {
    selectVarNationality = $('#nationality :selected').text();
//    console.log(selectvarnationality);
});


fetchUserInfo(selectVarNationality, radioVarGendre);                  //function call to start with the default ie female name from france


/*using the async/wait mtd to handle async function */
async function fetchUserInfo(nationality, gender){ 
    
           const url = "https://randomuser.me/api/?gender="+gender+"&nat=" + nationality;


 try                                                                  //using try and catch mtd to handle the errors/rejected promises that can araise during async actions
       { 
           const response = await fetch(url);
        
           const userDataNameNation = await response.json();           //to get the json body from response
       
       
        /* extract the title and name from the json body and display in h0 element*/ 
         $("h0").html("");                                             //for removing the element
            
         /*inserting element <div> dynamically to html using jquery */
            p = `<div>
            <h0 >${userDataNameNation.results[0].name.title}
            ${userDataNameNation.results[0].name.first}</h0>
            </div>`;
            $("#results").append(p);
            /*
            console.log(`${userDataNameNation.results[0].name.first}`);
            console.log(`${userDataNameNation.results[0].gender}`);
            console.log(`${userDataNameNation.results[0].location.country}`);
            */

       }catch(err){
           alert(err);
       }
    }

    /*if you want to retain the same value for male and female then this is a button which by clicking you will just get new names  */
generateAnotherName = `<button id="loadmore" class = "btn btn-primary"> Generate Name </button>`;

            $("#results").append(generateAnotherName);
            $('#loadmore').on("click", ()=>{
            fetchUserInfo(selectVarNationality, radioVarGendre);
    });
})




