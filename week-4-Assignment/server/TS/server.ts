import { createServer, IncomingMessage, ServerResponse } from 'http';
import { parse } from 'url';
import { Route } from './route'
export class Server {


    constructor(public port: number, public host: string) {
    }

    createServer() {
        const server = createServer(function (req: IncomingMessage, res: ServerResponse) {
            let parsedURL = parse(req.url, true);
            let path = parsedURL.pathname;
            path = path.replace(/^\/+|\/+$/g, "");
            let method: any = req.method.toLowerCase();
            if (method === 'post') {
                let reqBody = '';
                req.on('data', (chunk: string) => {
                    reqBody += chunk;
                })
                req.on("end", function () {
                    console.log(reqBody);
                    route(reqBody, path, res, method)
                });
            }
            else if (method === 'get') {
                let reqBody = JSON.stringify(parsedURL.query);
                route(reqBody, path, res, method)

            }
        });
        server.listen(this.port, this.host, () => {
            console.log("Listening on port 1234");
        });
    }
}

let server = new Server(1234, 'localhost');
server.createServer();
function route(reqBody: any, path: string, res: ServerResponse, method: string) {
    switch (path) {
        case 'api/createUser': {
            let route = new Route(path, reqBody);
            route.createDirectory();
            route.createFile();
            route.saveDataToMongoDb();
            let responseHeaderDetails = {
                "Content-Type": "text/html",
                "Access-Control-Allow-Origin": "*"
            }
            res.writeHead(200, responseHeaderDetails);
            res.write('Your data has been saved in the database');
            res.end("\n");
        }
            break;
        case 'api/user/userName': {
            switch (method) {
                case 'get': {
                    let route = new Route(path, reqBody, res);
                    let item = route.searchDataFromMongoDb();

                }
                    break;
                case 'post': { }
                    break;
            }
        }
            break;
    }
}

