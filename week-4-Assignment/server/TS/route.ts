import * as fs from 'fs';
import * as mongo from 'mongodb'
import { createServer, IncomingMessage, ServerResponse } from 'http';
var MongoClient = require('mongodb').MongoClient;

export class Route {
    routePath: string;
    reqBody: any;
    res: ServerResponse;

    constructor(path: string, reqBody: object, res?: ServerResponse) {
        this.routePath = path;
        this.res = res
        this.reqBody = reqBody;
        this.reqBody = JSON.parse(this.reqBody);
    }

    createDirectory() {
        if (fs.existsSync(`./userData/${this.reqBody.country}/${this.reqBody.state}/${this.reqBody.name}`)) {
            console.log('path exists');
        }
        else {
            fs.mkdirSync(`./userData/${this.reqBody.country}/${this.reqBody.state}/${this.reqBody.name}`, { recursive: true });
        }
    }

    createFile() {
        fs.writeFile(`./userData/${this.reqBody.country}/${this.reqBody.state}/${this.reqBody.name}/about.json`, `${this.reqBody.about}`, (err) => {
            if (err) throw err;
        })
    }

    async getConnection(url: string, dbname: string) {
        return new Promise((resolve, reject) => {
            let dbConnection = null;
            try {
                let uri: string = 'mongodb://localhost:27017';
                MongoClient.connect(uri, (err: string, client: mongo.MongoClient) => {
                    dbConnection = client.db(dbname);
                    resolve(dbConnection);
                });
            } catch (e) {
                reject(e);
            }
        });
    }

    async saveDataToMongoDb() {
        let db: any = await this.getConnection('mongodb://localhost:27017', 'usersDb');
        const collection = db.collection('userDescription');
        collection.insertOne({ name: `${this.reqBody.name}`, country: `${this.reqBody.country}`, state: `${this.reqBody.state}`, about: `${this.reqBody.about}` }, (err: any, result: any) => {
            collection.createIndex({ name: 1 });
        })
    }

    async searchDataFromMongoDb() {
        let db: any = await this.getConnection('mongodb://localhost:27017', 'usersDb');
        const collection = db.collection('userDescription');
        collection.find({ name: `${this.reqBody.searchName}` }).toArray((err: mongo.MongoError, items: Array<string>) => {
            let responseHeaderDetails = {
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*"
            }
            this.res.writeHead(200, responseHeaderDetails);
            this.res.write(`${JSON.stringify(items)}`);
            this.res.end("\n");
        })
    }
}