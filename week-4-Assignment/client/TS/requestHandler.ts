import $ from "jquery"

class Client{

formDataHandler(){
let displayResponseOnUI = "";
const myForm = document.getElementById('myForm');
const myFormSearch = document.getElementById('myFormSearch');
const form = document.forms.namedItem("myForm")
const formSearch = document.forms.namedItem("myFormSearch")
myForm.addEventListener('submit', function (e) {
e.preventDefault();
const formData = new FormData(form);
let requestObject = {
            "name": `${formData.get('fname')}`,
            "country": `${formData.get('country')}`,
            "state": `${formData.get('state')}`,
            "about": `${formData.get('about')}`
        };
        fetch('http://localhost:1234/api/createUser', {
            method: 'post',
            body: `${JSON.stringify(requestObject)}`
        }).then(response => {console.log(response);
        return response.text()}).then(data => {
            console.log(data)
            displayResponseOnUI = `<div>
                <h0 > 
                ${JSON.stringify(data)}
                <br></br>
                 </h0>
                </div>`;
            $("#results").append(displayResponseOnUI);
        })
    })
myFormSearch.addEventListener('submit', function (e) {
        e.preventDefault();
       const formData = new FormData(formSearch);
        let requestObject = {
            "searchName": `${formData.get('searchName')}`
        };
        fetch(`http://localhost:1234/api/user/userName?searchName=${formData.get('searchName')}`, {
            method: 'get',
           // body: `${JSON.stringify(requestObject)}`
        }).then(response => {console.log(response);
        return response.json()}).then(data => {
            for (let respons of data){
            displayResponseOnUI = `<div style="background-color: gray">
                <h0 > 
                ${respons.name} from ${respons.state}, ${respons.country} says ${respons.about}
                <br></br>
                </h0>
                </div>`;
            $("#results1").append(displayResponseOnUI);
            }
        })
    })
}
}

let client = new Client();
client.formDataHandler();