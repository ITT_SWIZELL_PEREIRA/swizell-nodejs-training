The objective of this assignment is to create a server to process POST request to save the data that a user enters in an array and also create folders and file 
and save data in that file based on user input. (without Express)
for eg: if user enters country/state/name as usa/boston/swizell then a file about.txt in folder usa/boston/swizell will be created.

The server was created using createServer method in http module. The user has to enter the data through UI. The request sent is POST. I used fetch to send the request
to the server. I made a separate folder UserData to store the about files of users. 
 