const http = require("http");
const url = require("url");
const server = http.createServer(function (req, res) {
  let parsedURL = url.parse(req.url, true);
  let path = parsedURL.pathname;
  path = path.replace(/^\/+|\/+$/g, "");
  let queryString = parsedURL.query;
  let headers = req.headers;
  let method = req.method.toLowerCase();
  let reqBody = '';
  req.on('data', chunk => {
    reqBody += chunk;
  })
  req.on("end", function () {
    let route =
      typeof routes[path] !== "undefined" ? routes[path] : routes["notFound"];
    let data = {
      path: path,
      queryString: queryString,
      headers: headers,
      method: method
    };
    route(data, res, reqBody);
  });
});
server.listen(1234, function () {
  console.log("Listening on port 1234");
});
let routes = require('./route.js');