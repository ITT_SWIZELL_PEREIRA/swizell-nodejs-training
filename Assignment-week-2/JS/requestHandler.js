$(document).ready(function(){
let displayResponseOnUI = "";
const myForm = document.getElementById('myForm');
myForm.addEventListener('submit', function(e){
e.preventDefault();
const formData = new FormData(this); 
let requestObject = {
            "name": `${formData.get('fname')}`,
            "country": `${formData.get('country')}`,
            "state": `${formData.get('state')}`,
            "about" : `${formData.get('about')}`
        };
fetch('http://localhost:1234/api/createUser', {
        method: 'post',
        body: `${JSON.stringify(requestObject)}`
    }).then(response=>response.json()).then(data=>{
        displayResponseOnUI = `<div>
            <h0 > Your data has been saved successfully in the following array.
            <br></br>
            ${JSON.stringify(data)}
            <br></br>
             </h0>
            </div>`;
            $("#results").append(displayResponseOnUI);
})
})
})