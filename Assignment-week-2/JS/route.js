const http = require("http");
const url = require("url");
const fs = require("fs");

module.exports = {
  'api/createUser': function (data, res, reqBody) {
    let reqBodyObj = JSON.parse(reqBody);
    directoryPath = `./../UserData/${reqBodyObj.country}/${reqBodyObj.state}/${reqBodyObj.name}`
    fs.access(directoryPath, (pathNotDefined) => {
      if (pathNotDefined) {
        fs.mkdir(`./../UserData/${reqBodyObj.country}/${reqBodyObj.state}/${reqBodyObj.name}`, { recursive: true }, (err) => {
          if (err) {
            return console.error(err.message);
          }
          fs.writeFile(`./../UserData/${reqBodyObj.country}/${reqBodyObj.state}/${reqBodyObj.name}/about.json`, `${reqBodyObj.about}`, err => {
            if (err) throw err.message;

          })
        });
      }
      let users = require('./user.js');
      users.push(reqBodyObj);
      fs.writeFile('./user.js', `module.exports = users = ${JSON.stringify(users)}`, function (err) {
        if (err) throw err;
        let responseHeaderDetails = {
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin": "*"
        }
        res.writeHead(200, responseHeaderDetails);
        res.write(`${JSON.stringify(users)}`);
        res.end("\n");
      })

    })

  }
};