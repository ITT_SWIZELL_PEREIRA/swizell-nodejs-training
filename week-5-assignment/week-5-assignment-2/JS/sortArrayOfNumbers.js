const spawn = require('child_process').spawn;
if (process.argv[2] === 'child') {
  spawn(process.execPath, [__filename, 'child'])
  let array = new Array(6, 2, 3, 9);
  let sorted = array.sort();
  console.log('sorted array is: ' + sorted);
}
else {
  let child = spawn(process.execPath, [__filename, 'child']);
  child.stdout.on('data', function (data) {
    console.log('from child process:', data.toString());
  })
}