import express from 'express';
const bodyParser = require('body-parser');
const fs = require('fs')
var path = require('path')
import * as mongo from 'mongodb'
var morgan = require('morgan');
var helmet = require('helmet')
var MongoClient = require('mongodb').MongoClient;

export class Route {
    server: express.Application;
    constructor(server: express.Application) {
        this.server = server;
    }

    serverStaticFiles() {
        this.server.use(express.static(__dirname + '/../../client'));
    }

    logginMiddleware() {
        var accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), { flags: 'a' })
        this.server.use(morgan('combined', { stream: accessLogStream }));
        this.server.use(morgan("combined"));
    }

    crudOperations() {
        this.server.use(helmet());
        this.logginMiddleware();
        this.server.get('/api/user/userName', (req: express.Request , res: express.Response) => {
            var queryParameter = req.query;
            req.headers['content-type'] = 'application/x-www-form-urlencoded';
            console.log(req.headers);
            this.searchDataFromMongoDb(queryParameter, res);
        })

        this.server.get('/registration.html', (req, res) => {
            this.serverStaticFiles();
        });
        let urlencodedParser = bodyParser.urlencoded({ extended: false })

        this.server.post('/api/createUser', urlencodedParser, (req: express.Request, res: express.Response) => {
            req.headers['content-type'] = 'application/x-www-form-urlencoded';
            let response = {
                name: req.body.fname,
                country: req.body.country,
                state: req.body.state,
                about: req.body.about
            };
            res.set('Content-Type', 'text/html');
            res.send('your data has be added to database');

            if (fs.existsSync(`./userData/${response.country}/${response.state}/${response.name}`)) {
                console.log('path exists');
            }
            else {
                fs.mkdirSync(`./userData/${response.country}/${response.state}/${response.name}`, { recursive: true });
            }

            fs.writeFile(`./userData/${response.country}/${response.state}/${response.name}/about.json`, `${response.about}`, (err: any) => {
                if (err) throw err;
            })
            this.saveData(response);
        })
    }

    async getConnection(url: string, dbname: string) {
        return new Promise((resolve, reject) => {
            let dbConnection = null;
            try {
                let uri: string = 'mongodb://localhost:27017';
                MongoClient.connect(uri, (err: string, client: mongo.MongoClient) => {
                    dbConnection = client.db(dbname);
                    resolve(dbConnection);
                });
            } catch (e) {
                reject(e);
            }
        });
    }

    async saveData(response: any) {
        let db: any = await this.getConnection('mongodb://localhost:27017', 'usersDb');
        const collection = db.collection('userDescription');
        collection.insertOne({ name: `${response.name}`, country: `${response.country}`, state: `${response.state}`, about: `${response.about}` }, (err: any, result: any) => {
            collection.createIndex({ name: 1 });
        })
    }

    async searchDataFromMongoDb(reqBody: any, res: express.Response) {
        let db: any = await this.getConnection('mongodb://localhost:27017', 'usersDb');
        const collection = db.collection('userDescription');
        collection.find({ name: `${reqBody.searchName}` }).toArray((err: mongo.MongoError, items: any) => {
            res.set('Content-Type', 'application/json')
            for (var respons of items) {
                res.write(`${respons.name} from ${respons.state}, ${respons.country} says ${respons.about}\n`);
            }
            res.end("\n");
        })
    }
}