import express from 'express';
import {Route} from './route';
class Server{
    constructor(public port: number, public host: string) {
    }

    createServer() {
        const server = express();
        let route = new Route(server);
        route.crudOperations();
       server.listen(this.port, this.host, ()=>{
            console.log(`server running at http://localhost:${this.port}`);
        })
    }
}

let server = new Server(1234, 'localhost');
server.createServer();