var gulp = require("gulp");
var ts = require("gulp-typescript");
var nodemon = require('gulp-nodemon');
var tsProject = ts.createProject("tsconfig.json", {
    "allowSyntheticDefaultImports": true
});

gulp.task("compileServer", function () {
    return gulp.src('server/TS/**/*.ts').pipe(tsProject()).pipe(gulp.dest("server/JS"));
});

//gulp.task("compileClient", function () {
  //  return gulp.src('client/TS/**/*.ts').pipe(tsProject()).pipe(gulp.dest("client/JS"));
//});

gulp.task('nodemon', function () {
    nodemon({
        script: './server/JS/server.js',
        ext: 'js, json, ts',
        env: { 'NODE_ENV': 'development' }
    });
});
